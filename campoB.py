from scipy.interpolate import interp1d
import numpy as np

f = open('data/B_z_1000A.txt','r')
x = []
y = []
for l in f:
    l = l.strip()
    d = l.split('      ')
    x.append(float(d[0]))
    y.append(float(d[1]))
    
x = (np.array(x) +0.64)*100
y = np.array(y)  *1e4 #in gauss
#now we have the field at 1000A 

def getB(current):
    y_scaled = y *  (current/ 1000)
    #returning the interpolation funct
    return interp1d(x, y_scaled)
    

