︠8f4750a2-3db3-49a9-b873-ab75de105645s︠
#libreria per grafici decenti
import plotly.plotly as py #richiede la connessione al loro server
import plotly.graph_objs as go #sono gli oggetti grafici su cui lavoriamo
py.sign_in('gianluca.rigoletti', 'uk20a8x0i8')
︡d320446d-5218-4e4e-88ff-c60feeee548d︡︡{"done":true}
︠2d93b044-d839-4d3d-884b-2ad1a2f2e55fs︠

import pandas as pd     #serve per le colonne di dati
from scipy.odr import * #serve per il fitting
from scipy import stats #serve per la pdf del chi2
︡194f4a5b-dc0e-4acd-9b7b-fa8ca3d39404︡︡{"done":true}
︠b72b5765-041f-43ff-baf6-eeb6b1dc341fs︠
#Definisco le variabili statistiche che userò poi
resistenza       = 0
sigma_resistenza = 0
q                = 0 #parametro dell'intercetta
sigma_q          = 0 
dof              = 0
chi2             = 0 #chi quadro
chi2ridotto      = 0 #chi quadro ridotto
prob             = 0 #probabilità dalla distribuzione del chi quadro

df = pd.read_csv('misure_sonda_1.csv') #df = dataframe

df.columns = ['I', 'V', 'sI', 'sV'] #Imposto il nome delle colonne

#mi servono gli array con i dati
x = df.I
y = df.V
sx = df.sI
sy = df.sV

#definisco la funzione di fit
def f(p, x):
    return p[0] + p[1]*x

#ipotizzo il modello di fit lineare
linear = Model(f)
mydata = RealData(x, y, sx, sy) #includo anche gli errori, rispettivamente sx e sy
myodr =  ODR(mydata, linear, beta0=[0., 5.]) #imposto anche dei parametri iniziali per indovinare il fit
myoutput = myodr.run() #fit!
myoutput.pprint() #prettyprint: stampo i risultati per sicurezza
︡50183a46-2ef9-486b-9201-f8ab66f45583︡︡{"stdout":"Beta: [ -3.05846434e-04   5.02124486e-01]\nBeta Std Error: [  1.24745634e-04   1.94467023e-05]\nBeta Covariance: [[  1.65787075e-07  -2.26403964e-08]\n [ -2.26403964e-08   4.02895010e-09]]\nResidual Variance: 0.0938642127451\nInverse Condition #: 0.00747160697008\nReason(s) for Halting:\n  Sum of squares convergence\n"}︡{"done":true}
︠b7482036-4fb2-40ce-a4a1-1fcefcffe424s︠
#salvo e calcolo i dati che uso poi per la legenda
resistenza       = myoutput.beta[1]
sigma_resistenza = myoutput.sd_beta[1]
q                = myoutput.beta[0]
sigma_q          = myoutput.sd_beta[0]

#calcolo della probabilità del chi2
chi2             = myoutput.sum_square
chi2ridotto      = myoutput.res_var
dof              = chi2 / chi2ridotto
prob             = 1 - stats.chi2.cdf(chi2, dof) #cdf è la probabilità cumulativa
︡6f0e3792-bdf5-43dc-8125-5d68b002431d︡︡{"done":true}
︠fe1d571c-13e7-4953-8e0d-497a3ba4ae9d︠
#Traccio i punti misurati e li disegno
trace = go.Scatter(
    name='Dati',
    x=df.I, #imposto le colonne
    y=df.V,
    mode='markers', #uso i markers per tracciare i punti
    marker=dict(
        color='#85144b', #colori dei pallini
        size=8 # e la loro grandezza
    ),
    error_y=dict( #imposto gli errori
        type='data', #serve a dichiarare che gli erro ili prendo da un array
        array=df['sV'], #dichiaro l'array da cui li prendo
        thickness=1,
        width=0,
        color='#85144b',
        opacity=0.8
    ),
    error_x=dict( #imposto gli errori
        type='data',
        array=df['sI'],
        thickness=1,
        width=0,
        color='#85144b',
        opacity=0.8
    ),
    showlegend=True
)

#Già che ci sono traccio anche la curva teorica con un altro colore
# x lo ho già definito come un array. Mi basta definire y e sono a cavallo
y = resistenza * x
trace2 = go.Scatter(
    x=x,
    y=y,
    name='Andamento teorico',
    showlegend=False
)
#definisco il testo da inserire nel riquadro cone le statistiche. Purtroppo usare LaTeX rompe tutto quanto,
#quindi si usa un po' di HTML
text = ('P = {0} <br>χ<sup>2</sup>= {1}<br>χ<sup>2</sup>/dof= {2}<br>R = {3} ± {4}Ω<br>q= {5} ± {6}'
        .format(round(prob,3), round(chi2,3), round(chi2ridotto, 3), 
        round(resistenza, 3), round(sigma_resistenza, 5), round(q, 4), round(sigma_q, 4)))data = [trace2, trace] #aggiungo i grafici al canvas che voglio disegnare
layout = go.Layout(
    xaxis=dict(title='$I (mA)$'),
    yaxis=dict(title='$V (V)$'),
    title='Misure sonda 1',
    showlegend=True,
    annotations=[ 
        dict(
            text=text, #imposto il testo sopra definito 
            y=0.610445205479452, #posizione del riquadro
            x=1.0764500349406,
            xref="paper",
            yref="paper",
            showarrow=False,
            xanchor="auto",
            bgcolor="rgb(255, 255, 255)",
            borderpad=6,
            bordercolor="rgb(31, 119, 180)",
            font=dict(
                size=16,
                color="#7f7f7f"
            ),
            align="left"
        )
    ]
)
fig = go.Figure(data=data, layout=layout)

plot(x * sin(x), (x, -2, 10))









